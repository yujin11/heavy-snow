define(["require", "exports"], function (require, exports) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var Rain = /** @class */ (function () {
        function Rain(size, speed, image) {
            this.frameRate = 50;
            this.size = size;
            this.speed = speed;
            this.image = image;
            this.run();
        }
        Rain.prototype.run = function () {
            var _this = this;
            var windowWidth = window.outerWidth;
            var animationSpeed = 1000 / this.frameRate;
            var verticalStep = window.innerHeight / this.speed;
            this.element = document.createElement('div');
            document.body.appendChild(this.element);
            this.element.className = 'drop-item ' + this.size + ' ' + this.image;
            this.element.style.left = Math.floor(Math.random() * ((windowWidth - 150) - 0 + 1)) + 0 + 'px';
            this.element.style.top = '0px';
            var start = Date.now();
            // Animation
            var timer = setInterval(function () {
                var timePassed = Date.now() - start;
                if (timePassed >= _this.speed) {
                    clearInterval(timer);
                    _this.destroy();
                    return;
                }
                // Set top position
                _this.element.style.top = timePassed * verticalStep + 'px';
            }, animationSpeed);
        };
        Rain.prototype.destroy = function () {
            document.body.removeChild(this.element);
            this.size = null;
            this.speed = null;
            this.image = null;
            this.frameRate = null;
            this.element = null;
        };
        return Rain;
    }());
    exports.Rain = Rain;
});
//# sourceMappingURL=rainItem.js.map