export class Rain {
    private size: String;
    private speed: number;
    private image: String;
    private element: HTMLElement;
    private frameRate = 50;

    constructor(size: String,speed: number, image: String) {
        this.size = size;
        this.speed = speed;
        this.image = image;
        this.run();
    }

    private run() {
        let windowWidth = window.outerWidth;
        let animationSpeed = 1000 / this.frameRate;
        let verticalStep = window.innerHeight / this.speed;
        this.element = document.createElement('div');
        document.body.appendChild(this.element);
        this.element.className = 'drop-item ' + this.size + ' ' + this.image;
        this.element.style.left = Math.floor(Math.random() * ((windowWidth - 150) - 0 + 1)) + 0 + 'px';
        this.element.style.top = '0px';
        let start = Date.now();
        // Animation
        let timer = setInterval(()=> {
            var timePassed = Date.now() - start;
            if (timePassed >= this.speed) {
                clearInterval(timer);
                this.destroy();
                return;
            }
            // Set top position
            this.element.style.top = timePassed * verticalStep + 'px';
        }, animationSpeed);
    }

    private destroy() {
        document.body.removeChild(this.element);
        this.size = null;
        this.speed = null;
        this.image = null;
        this.frameRate = null;
        this.element = null;
    }
}
