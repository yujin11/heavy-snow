define(["require", "exports", "./snowItem", "./rainItem"], function (require, exports, snowItem_1, rainItem_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    var StartApp = /** @class */ (function () {
        function StartApp() {
            var _this = this;
            this.isShowSnow = true;
            this.snowInterval = null;
            this.rainInterval = null;
            this.changeBtn = document.getElementsByClassName('btn-wrap')[0].getElementsByTagName('button')[0];
            this.changeBtn.addEventListener('click', function () {
                _this.isShowSnow = !_this.isShowSnow;
                _this.run();
            });
            // Setting Snow size.
            this.snowSizeClasses = new Array();
            this.snowSizeClasses.push('small');
            this.snowSizeClasses.push('medium');
            this.snowSizeClasses.push('large');
            // Setting Snow rotations.
            this.snowRotationClasses = new Array();
            this.snowRotationClasses.push('no-rotation');
            this.snowRotationClasses.push('slow-rotation');
            this.snowRotationClasses.push('medium-rotation');
            this.snowRotationClasses.push('fast-rotation');
            // Setting Snow images.
            this.snowImageClasses = new Array();
            this.snowImageClasses.push('bg-image-1');
            this.snowImageClasses.push('bg-image-2');
            // Setting Snow speed.
            this.snowSnowSpeedTime = new Array();
            this.snowSnowSpeedTime.push(5000);
            this.snowSnowSpeedTime.push(9000);
            this.snowSnowSpeedTime.push(12000);
            // Setting Drop speed.
            this.snowDropSpeedTime = new Array();
            this.snowDropSpeedTime.push(500);
            this.snowDropSpeedTime.push(700);
            this.snowDropSpeedTime.push(900);
            this.snowDropSpeedTime.push(1100);
            this.snowDropSpeedTime.push(1300);
            this.snowDropSpeedTime.push(1500);
            this.snowDropSpeedTime.push(1700);
        }
        StartApp.prototype.run = function () {
            var _this = this;
            if (this.isShowSnow) {
                this.changeBtn.innerHTML = 'Show Rain';
                clearInterval(this.rainInterval);
                this.snowInterval = setInterval(function () {
                    _this.setRandomSnowItem();
                }, 500);
            }
            else {
                this.changeBtn.innerHTML = 'Show Snow';
                clearInterval(this.snowInterval);
                this.rainInterval = setInterval(function () {
                    _this.setRandomDropItem();
                }, 10);
            }
        };
        StartApp.prototype.setRandomSnowItem = function () {
            var className = this.snowSizeClasses[this.getRndInteger(0, this.snowSizeClasses.length - 1)];
            var rotationClass = this.snowRotationClasses[this.getRndInteger(0, this.snowRotationClasses.length - 1)];
            var speed = this.snowSnowSpeedTime[this.getRndInteger(0, this.snowSnowSpeedTime.length - 1)];
            var image = this.snowImageClasses[this.getRndInteger(0, this.snowImageClasses.length - 1)];
            new snowItem_1.Snow(className, rotationClass, speed, image);
        };
        StartApp.prototype.setRandomDropItem = function () {
            var className = this.snowSizeClasses[this.getRndInteger(0, this.snowSizeClasses.length - 1)];
            var speed = this.snowDropSpeedTime[this.getRndInteger(0, this.snowDropSpeedTime.length - 1)];
            var image = this.snowImageClasses[this.getRndInteger(0, this.snowImageClasses.length - 1)];
            new rainItem_1.Rain(className, speed, image);
        };
        StartApp.prototype.getRndInteger = function (min, max) {
            return Math.floor(Math.random() * (max - min + 1)) + min;
        };
        return StartApp;
    }());
    var app = new StartApp();
    app.run();
});
//# sourceMappingURL=main.js.map