import {Snow} from './snowItem'
import {Rain} from "./rainItem";

class StartApp {
    private snowSizeClasses: String[];
    private snowRotationClasses: String[];
    private snowImageClasses: String[];
    private snowSnowSpeedTime: number[];
    private snowDropSpeedTime: number[];
    private isShowSnow: boolean = true;
    private changeBtn: Element;
    private snowInterval = null;
    private rainInterval = null;

    constructor() {
        this.changeBtn = document.getElementsByClassName('btn-wrap')[0].getElementsByTagName('button')[0];
        this.changeBtn.addEventListener('click', ()=>{
           this.isShowSnow = !this.isShowSnow;
           this.run();
        });
        // Setting Snow size.
        this.snowSizeClasses = new Array();
        this.snowSizeClasses.push('small');
        this.snowSizeClasses.push('medium');
        this.snowSizeClasses.push('large');
        // Setting Snow rotations.
        this.snowRotationClasses = new Array();
        this.snowRotationClasses.push('no-rotation');
        this.snowRotationClasses.push('slow-rotation');
        this.snowRotationClasses.push('medium-rotation');
        this.snowRotationClasses.push('fast-rotation');
        // Setting Snow images.
        this.snowImageClasses = new Array();
        this.snowImageClasses.push('bg-image-1');
        this.snowImageClasses.push('bg-image-2');
        // Setting Snow speed.
        this.snowSnowSpeedTime = new Array();
        this.snowSnowSpeedTime.push(5000);
        this.snowSnowSpeedTime.push(9000);
        this.snowSnowSpeedTime.push(12000);
        // Setting Drop speed.
        this.snowDropSpeedTime = new Array();
        this.snowDropSpeedTime.push(500);
        this.snowDropSpeedTime.push(700);
        this.snowDropSpeedTime.push(900);
        this.snowDropSpeedTime.push(1100);
        this.snowDropSpeedTime.push(1300);
        this.snowDropSpeedTime.push(1500);
        this.snowDropSpeedTime.push(1700);
    }

    public run() {
        if(this.isShowSnow){
            this.changeBtn.innerHTML  = 'Show Rain';
            clearInterval(this.rainInterval);
            this.snowInterval = setInterval(()=> {
                this.setRandomSnowItem();
            }, 500);
        } else{
            this.changeBtn.innerHTML = 'Show Snow';
            clearInterval(this.snowInterval);
            this.rainInterval = setInterval(()=> {
                this.setRandomDropItem();
            }, 10);
        }
    }

    private setRandomSnowItem() {
        let className = this.snowSizeClasses[this.getRndInteger(0, this.snowSizeClasses.length - 1)];
        let rotationClass = this.snowRotationClasses[this.getRndInteger(0, this.snowRotationClasses.length - 1)];
        let speed = this.snowSnowSpeedTime[this.getRndInteger(0, this.snowSnowSpeedTime.length - 1)];
        let image = this.snowImageClasses[this.getRndInteger(0, this.snowImageClasses.length - 1)];
        new Snow(className, rotationClass, speed, image);
    }
    private setRandomDropItem() {
        let className = this.snowSizeClasses[this.getRndInteger(0, this.snowSizeClasses.length - 1)];
        let speed = this.snowDropSpeedTime[this.getRndInteger(0, this.snowDropSpeedTime.length - 1)];
        let image = this.snowImageClasses[this.getRndInteger(0, this.snowImageClasses.length - 1)];
        new Rain(className, speed, image);
    }

    private getRndInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

}

let app = new StartApp();
app.run();
